/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "My Project", "index.html", [
    [ "Motor Driver", "index.html#sec_mot", null ],
    [ "Encoder Driver", "index.html#sec_enc", null ],
    [ "Proportional Control", "index.html#sec_pro", null ],
    [ "IMU Driver", "index.html#sec_imu", null ],
    [ "IMU Testing", "IMUTesting.html", null ],
    [ "Proportional Control Tuning", "PropControlTuning.html", null ],
    [ "Term Project Report", "ProDet.html", [
      [ "Abstract", "ProDet.html#sec_abstract", null ],
      [ "Zack's Main Code", "ProDet.html#sec_MainCode", null ],
      [ "Austin's Video Demonstration", "ProDet.html#sec_AusVid", null ],
      [ "Zack's Video Demonstration", "ProDet.html#sec_ZackVid", null ],
      [ "Background", "ProDet.html#sec_back", null ],
      [ "Testing", "ProDet.html#sec_test", null ],
      [ "Conclusion", "ProDet.html#sec_conc", null ]
    ] ],
    [ "Term Project Proposal", "TermProp.html", null ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
".html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';